/* TD03 - Pawn++

Le but de cet exercice est de créer les classes
Pawn, King, Queen, Rook et Bishops correspondant
aux pièces d'un jeu d'échec.

Chaque pièce devra avoir un type, qui sera un
nombre correspondant à :

0 : Pawn
1 : King
2 : Queen
3 : Rook
4 : Bishop

Chacune de ces pièce devra pouvoir se déplacer
avec la méthode move(x, y) et sera initialisée
avec un x et un y.
(il n'est pas nécessaire de vérifier les déplacements).

*/

/* TD Part */

abstract class Piece {

    constructor(public readonly type: number, public x: number, public y: number) {}

    move(x: number, y: number): void {
        this.x = x;
        this.y = y;
    }
}

class Pawn extends Piece {

    constructor(x: number, y: number) {
        super(0, x, y);
    }
}
class King extends Piece {

    constructor(x: number, y: number) {
        super(1, x, y);
    }
}
class Queen extends Piece {

    constructor(x: number, y: number) {
        super(2, x, y);
    }
}
class Rook extends Piece {

    constructor(x: number, y: number) {
        super(3, x, y);
    }
}
class Bishop extends Piece {

    constructor(x: number, y: number) {
        super(4, x, y);
    }
}

/* Testing Part */

import { expect } from 'chai';
import 'mocha';


describe('TD03', () => {
    //
    function testPiece(piece: any, id: number) {
        const moveX = Math.floor(Math.random() * 10);
        const moveY = Math.floor(Math.random() * 10);
        expect(typeof (piece.move)).to.equal('function');
        expect(piece.type).to.equal(id);
        piece.move(moveX, moveY);
        expect(piece.x).to.equal(moveX);
        expect(piece.y).to.equal(moveY);
    }
    const p : any = new Pawn(0, 0);
    it('Should contain a Pawn that can move', () => {testPiece(p, 0)});
    const k = new King(1, 1);
    it('Should contain a King that can move', () => {testPiece(k, 1)});
    const q = new Queen(2, 2);
    it('Should contain a Queen that can move', () => {testPiece(q, 2)});
    const r = new Rook(2, 2);
    it('Should contain a Rook that can move', () => {testPiece(r, 3)});
    const b = new Bishop(3, 3);
    it('Should contain a Queen that can move', () => {testPiece(b, 4)});
});